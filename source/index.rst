Les piles
==========

.. image:: img/intro_pile.jpeg
    :align: center
    :class: margin-bottom-16

Contenus
-----------

-   Structures de données, interface et implémentation.
-   Pile : structure linéaire.

Capacités attendues
--------------------

-   Spécifier une structure de données par son interface.
-   Distinguer interface et implémentation
-   Écrire plusieurs implémentations d'une même structure de données.
-   Distinguer des structures par le jeu des méthodes qui les caractérisent.
-   Choisir une structure de données adaptées à la situation à modéliser.

.. toctree::
    :maxdepth: 1
    :hidden:

    content/pile.rst
    content/exercice.rst
    content/tp_ponctuation.rst
    content/ep_pr_1.rst

