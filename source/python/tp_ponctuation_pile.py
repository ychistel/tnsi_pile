def creer_pile():
    return []

def est_vide(p):
    return p == []

def empiler(p,e):
    p.append(e)
    return p

def depiler(p):
    if not est_vide(p):
        e = p.pop()
        return e

def opp(x):
    # liste des signes de ponctuation ouvrant
    ponctuation_ouvrante = ['(',')','{','}','[',']','"',"'"]
    # liste des signes de ponctuation fermant dans le même ordre
    ponctuation_fermante = [')','(','}','{',']','[','"',"'"]
    # la variable i contient l'indice du caractère x dans la liste ponctuation_ouvrante
    i = ponctuation_ouvrante.index(x)
    return ponctuation_fermante[i]

def verifie(expression):
    P = creer_pile()
    for c in expression:
        if c == '(' or c == ')' or c == '[' or c == ']' or c == '{' or c == '}' or c == '"' or c == "'":
            if est_vide(P):
                empiler(P,c)
            else:
                s = depiler(P)
                if s != opp(c):
                #if (s != '(' or c != ')') and (s != '[' or c != ']') and (s != '{' or c != '}') and (s != '"' or c != '"') and (s != "'" or c != "'"):
                #if not ((s == '(' and c == ')') or (s == '[' and c == ']') or (s == '{' and c == '}') or (s == '"' and c == '"') or (s == "'" and c == "'")):
                    empiler(P,s)
                    empiler(P,c)
    #print(P)  
    return est_vide(P)

if __name__ == '__main__':
    e1 = "(4-(5+2))*3"
    e2 = "f(x)=[(x+1)^{2}-2]*(x-1)"
    e3 = "d = {'a':[1,2],'b':(3,4),'c':[(5,6),(7,8)]}"
    p1 = verifie(e1)
    p2 = verifie(e2)
    p3 = verifie(e3)
    e4 = "]x*(x+2[)"
    p4 = verifie(e4)
    e5 = ']-5;0[U]0;5['
    p5 = verifie(e5)