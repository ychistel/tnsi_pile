TP : Ponctuation
==================

Pour ce TP on utilise l'implémentation de pile construite avec une liste Python:

.. literalinclude:: ../python/pile.py
    :lines: 1-15

On rencontre en informatique et en mathématiques des expressions contenant des **ponctuations** comme les parenthèses, les crochets, des accolades et même les guillemets (quotes). 

Par exemple, en python, les structures de données comme les listes ou les dictionnaires utilisent des crochets et des accolades. 

Comment vérifier que la ponctuation d'une expression est correcte ?

#.  On donne trois expressions avec ponctuation.

    a.  ``(4-(5+2))*3``
    b.  ``f(x)=[(x+1)^{2}-2]*(x-1)``
    c.  ``d = {'a':[1,2],'b':(3,4),'c':[(5,6),(7,8)]}``

    Comment peut-on s'assurer que la ponctuation est correcte dans ces expressions ?

#.  On considère que chaque expression est une chaine de caractères. Écrire en python la fonction ``verifie`` qui prend en paramètre une chaine de caractères et qui renvoie une pile contenant toute la ponctuation de l'expression. 

    Par exemple, si on applique le script aux trois expressions ci-dessus, on obtient les piles suivantes:

    a.  ``['(', '(', ')', ')']``
    b.  ``['(', ')', '[', '(', ')', '{', '}', ']', '(', ')']``
    c.  ``['{', "'", "'", '[', ']', "'", "'", '(', ')', "'", "'", '[', '(', ')', '(', ')', ']', '}']``

#.  Une pile est une structure qui convient très bien pour vérifier la ponctuation d'une expression.

    Voici l'algorithme qui utilise une pile pour vérifier qu'une expression est correcte:

    .. code:: text

        Pour chaque ``caractère`` de l'expression:
            si le ``caractère`` est une ponctuation:
                si la ``pile`` est vide:
                    on ajoute le ``caractère`` dans la ``pile``
                sinon:
                    on prend le ``sommet`` de la ``pile``
                    si ``sommet`` et ``caractère`` ne correspondent pas:
                        on ajoute à la ``pile`` le ``sommet``
                        on ajoute à la ``pile`` le ``caractère``
        # on a finit l'expression
        si la ``pile`` est vide:
            on renvoie True
        sinon:
            on renvoie False
    
    Modifier la fonction ``verifie`` en utilisant une pile de façon à renvoyer un ``booléen`` qui affirme que l'expression est bien ponctuée ou non.

.. rubric:: Allez plus loin

On peut améliorer le code produit sur les points suivants:

#.  le test de vérification de la correspondance des signes de ponctuation peut être remplacé par un appel à une fonction ``corespondance(p1,p2)`` qui renvoie un booléen. La fonction renvoie ``True`` si les deux signes de ponctuation sont ouvrants et fermants et ``False`` dans le cas contraire.

#.  En l'état, certaines expressions bien ponctuées ne passent pas le test de vérification. Par exemple avec l'expression ``]-5;0[U]0;5[``. 

    a.  En admettant qu'un signe de ponctuation ouvrant a pour signe de ponctuation fermant son symbole symétrique, écrire une fonction ``opp(x)`` qui renvoie le signe symétrique du signe de ponctuation ``x``.
    b.  Modifier le test de vérification en utilisant la fonction ``opp`` pour élargir les expressions à vérifier.